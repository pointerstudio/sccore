/*
===========================================================================
scCore

Copyright (C) 2016 LUSTlab
This file is part of the scCore code method collection for SuperCollider
scCore is free software: you can redistribute it and/or modify
it under the terms of the ** whatever license **

Collected by: Jakob Schlötter,  jakob@lustlab.net

===========================================================================
*/

/*
================================================
load samples in buffer
================================================
*/


/*
=============
load samples from specific folder in buffer

to load files with specific names in "path" in seperate buffers, starting with 12
~loadBuffers.value("path/buffer_*.wav", 12)
=============
*/

~loadBuffers = { arg folder, startBufNum = 0;
	folder.asString.pathMatch.do({ arg elem, i;
		var bufNum = startBufNum + i;
		("loading buf " ++ bufNum ++" : " ++ elem).postln;
		Buffer.read(s, elem,bufnum:bufNum);
	});
};