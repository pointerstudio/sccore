// main manager
(
q = { ~midiInOn.free; ~midiInOff.free; ~midiInCC.free; ~midiManager.free; };
q.value;

~midiVerbose = True;

~midiPads.free;
~midiPads = [
	[9,10,11,12,25,26,27,28],
	[36,37,38,39,40,41,42,43],
	[36,37,38,39,40,41,42,43],
	[48,50,52,53,55,57,59,60,62,64,65,67,69,71,72]
];
~midiKnobs.free;
~midiKnobs = [
	[31,41,32,42,33,43,34,44,35,45,36,46,37,47,38,48],
	[1,2,3,4,5,6,7,8],
	[1,2,3,4,5,6,7,8]
];

~synths = [];

~stopAllSynths = {
	~pad2.do({ arg item, i;
		~synths = ~synths.add([1,~midiPads[1][5],`(~pad2[i])]);
	});
	~synths.do({ arg item, i;
		var chan = item[0];
		var num = item[1];
		var veloc = 0;
		item[2].value.set(\fadeTime, 0.5);
		~noteOff.value(chan,num,veloc);
		~launchControl.noteOn(chan,num,'29'); // red l red'13' amber'29' green'28'
	});
	~synths = [];
};

// CHANNEL 0
~midiManager0.free;
~midiManager0 = { arg type, index, value;

	// pads
	// 1
	if(index == ~midiPads[0][0], {
		if(type == 1, {
			~buf1 = Synth.new(\playmonobufloop).set(\bufnum,1).set(\fadeTime,5).set(\amp,0.0).set(\rate,0.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf1]);
			}, {
				~buf1.release();
		});
	});

	// 2
	if(index == ~midiPads[0][1], {
		if(type == 1, {
			~buf2 = Synth.new(\playmonobufloop).set(\bufnum,2).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf2]);
			}, {
				~buf2.release();
		});
	});
	// 3
	if(index == ~midiPads[0][2], {
		if(type == 1, {
			~buf3 = Synth.new(\playmonobufloop).set(\bufnum,3).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf3]);
			}, {
				~buf3.release();
		});
	});

	// 4
	if(index == ~midiPads[0][3], {
		if(type == 1, {
			~buf4 = Synth.new(\playmonobufloop).set(\bufnum,4).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf4]);
			}, {
				~buf4.release();
		});
	});

	// 5
	if(index == ~midiPads[0][4], {
		if(type == 1, {
			~buf5 = Synth.new(\playmonobufloop).set(\bufnum,5).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf5]);
			}, {
				~buf5.release();
		});
	});
	// 6
	if(index == ~midiPads[0][5], {
		if(type == 1, {
			~buf6 = Synth.new(\playmonobufloop).set(\bufnum,6).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf6]);
			}, {
				~buf6.release();
		});
	});
	// 7
	if(index == ~midiPads[0][6], {
		if(type == 1, {
			~buf7 = Synth.new(\playmonobufloop).set(\bufnum,7).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf7]);
			}, {
				~buf7.release();
		});
	});
	// 8
	if(index == ~midiPads[0][7], {
		if(type == 1, {
			~buf8 = Synth.new(\playmonobufloop).set(\bufnum,8).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([0,index,`~buf8]);
			}, {
				~buf8.release();
		});
	});

	// knobs
	// K1-1
	if(index == ~midiKnobs[0][0], {
		~buf1.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 2));
	});
	// K1-2
	if(index == ~midiKnobs[0][1], {
		~buf1.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});

	// K2-1
	if(index == ~midiKnobs[0][2], {
		~buf2.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 2));
	});
	// K2-2
	if(index == ~midiKnobs[0][3], {
		~buf2.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});

	// K3-1
	if(index == ~midiKnobs[0][4], {
		~buf3.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 1.0));
	});
	// K3-2
	if(index == ~midiKnobs[0][5], {
		~buf3.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});

	// K4-1
	if(index == ~midiKnobs[0][6], {
		~buf4.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 1.0));
	});
	// K4-2
	if(index == ~midiKnobs[0][7], {
		~buf4.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});

	// K5-1
	if(index == ~midiKnobs[0][8], {
		~buf5.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 1.0));
	});
	// K5-2
	if(index == ~midiKnobs[0][9], {
		~buf5.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});

	// K6-1
	if(index == ~midiKnobs[0][10], {
		~buf6.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 1.0));
	});
	// K6-2
	if(index == ~midiKnobs[0][11], {
		~buf6.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});

	// K7-1
	if(index == ~midiKnobs[0][12], {
		~buf7.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 1.0));
	});
	// K7-2
	if(index == ~midiKnobs[0][13], {
		~buf7.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});

	// K8-1
	if(index == ~midiKnobs[0][14], {
		~buf8.set(\rate, LinLin.kr(value.asFloat, 0, 127, 0.0, 1.0));
	});
	// K8-2
	if(index == ~midiKnobs[0][15], {
		~buf8.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});
};

// CHANNEL 1
~midiManager1 = { arg type, index, value;
	// pads
	// 1
	if(index == ~midiPads[1][0], {
		if(type == 1, {
			~buf1_1 = Synth.new(\playmonobufloop)
			.set(\bufnum,15)
			.set(\fadeTime,5)
			.set(\amp,0.0)
			.set(\rate,1.0)
			.set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf1_1]);
			}, {
				~buf1_1.release;
		});
	});

	// 2
	if(index == ~midiPads[1][1], {
		if(type == 1, {
			~buf1_2 = Synth.new(\playmonobufloop)
			.set(\bufnum,16)
			.set(\fadeTime,5)
			.set(\amp,0.0)
			.set(\rate,1.0)
			.set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf1_2]);
			}, {
				~buf1_2.release;
		});
	});

	// 3
	if(index == ~midiPads[1][2], {
		if(type == 1, {
			~t3 = Synth.new(\tweet3).set(\fadeTime,10.0);
			~synths = ~synths.add([1,index,`~t3]);
			}, {
				~t3.set(\fadeTime,0.5).release;
		});
	});
	// 4
	if(index == ~midiPads[1][3], {
		if(type == 1, {
			Synth(\SOSkick, [\sustain, 0.5, \beater_noise_level, 0.005, \mod_freq, 5, \amp, 0.2])
			}, {
				Synth(\SOSkick, [\sustain, 0.2, \beater_noise_level, 0.01, \mod_freq, 20, \amp, 0.15])
		});
	});
	// 5
	if(index == ~midiPads[1][4], {
		if(type == 1, {
			~routine1Pad = True;
			if(~routine1Ton == False, {
				~routine1.play;
			});
			~synths = ~synths.add([1,index,`~pad]);
			}, {
				~routine1Pad = False;
				~pad.release();
				if(~routine1Ton == False, {
					~routine1.stop;
				});
		});
	});

	// 6
	if(index == ~midiPads[1][5], {
		if(type == 1, {
			~routine2Pad = True;
			if(~routine2Ton == False, {
				~routine2.play;
			});
/*			~pad2.do({ arg item, i;
			      ~synths = ~synths.add([1,index,`(~pad2[i])]);
			});*/
			}, {
				~routine2Pad = False;

				~pad2.do({ arg item, i;
					~pad2[i].release();
				});
				if(~routine2Ton == False, {
					~routine2.stop;
				});
		});
	});

	// 7
	if(index == ~midiPads[1][6], {
		// BPF.ar(~firegen,  600, 1/0.2) +
		// BPF.ar(~firegen, 1200, 1/0.6) +
		// BPF.ar(~firegen, 2600, 1/0.4) +
		if(type == 1, {
			~crackle1 = Synth.new(\crackle).set(\fadeTime, 2.0).set(\amp,0.0).set(\freq,600).set(\rq,1/0.2);
			~crackle2 = Synth.new(\crackle)
			.set(\fadeTime, 2.0).set(\amp,0.0).set(\freq,1200).set(\rq,1/0.6);
			~crackle3 = Synth.new(\crackle)
			.set(\fadeTime, 2.0).set(\amp,0.0).set(\freq,2600).set(\rq,1/0.4);
			~synths = ~synths.add([1,index,`~crackle1]);
			~synths = ~synths.add([1,index,`~crackle2]);
			~synths = ~synths.add([1,index,`~crackle3]);
			}, {
				~crackle1.release;
				~crackle2.release;
				~crackle3.release;
		});
	});
	// 8
	if(index == ~midiPads[1][7], {
		if(type == 1, {
			~modal = Synth.new(\modal).set(\fadeTime, 2.0).set(\amp,0.0).set(\peep,1.0);
			~synths = ~synths.add([1,index,`~modal]);
			}, {
				~modal.release;
		});
	});

	// knobs
	// K1
	if(index == ~midiKnobs[1][0], {
		~padamp = LinLin.kr(value.asFloat, 0, 127, 0, 2);
		~pad.set(\amp, ~padamp);
	});
	// K2
	if(index == ~midiKnobs[1][1], {
		~pad2amp = LinLin.kr(value.asFloat, 0, 127, 0, 2);
		~pad2.do({ arg item, i;
			~pad2[i].set(\amp, ~pad2amp);
		});
	});
	// K3
	if(index == ~midiKnobs[1][2], {
		~crackle1.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 2));
		~crackle2.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 2));
		~crackle3.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 2));
	});
	// K4
	if(index == ~midiKnobs[1][3], {
		~modal.set(\peep, LinLin.kr(value.asFloat, 0, 127, 0, 15));
	});
	// K5
	if(index == ~midiKnobs[1][4], {
		~buf1_1.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0.0, 2.0));
	});
	// K6
	if(index == ~midiKnobs[1][5], {
		~buf1_2.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0.0, 2.0));
	});
	// K7
	if(index == ~midiKnobs[1][6], {
		~t3.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 2));
	});
	// K8
	if(index == ~midiKnobs[1][7], {
		~modal.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});
};

// CHANNEL 2
~midiManager2 = { arg type, index, value;
	// pads
	// 1
	if(index == ~midiPads[2][0], {
		if(type == 1, {
			~stopAllSynths.value();
			}, {
		});
	});

	// 2
	if(index == ~midiPads[2][1], {
		if(type == 1, {
			~stopAllSynths.value();
			Synth(\SOSkick, [\sustain, 0.5, \beater_noise_level, 0.1, \mod_freq, 5, \amp, 0.25]);
 Synth.new(\playmonobuf).set(\bufnum,17).set(\fadeTime,0.125).set(\amp,0.25).set(\rate,10.0).set(\dur,0.5).set(\pos,[1,1,1,1]);
 Synth.new(\playmonobuf).set(\bufnum,18).set(\fadeTime,0.5).set(\amp,0.075).set(\rate,1.0).set(\dur,4.0).set(\pos,[1,1,1,1]);
 Synth.new(\playmonobuf).set(\bufnum,18).set(\fadeTime,1.0).set(\amp,0.075).set(\rate,2.0).set(\dur,2.0).set(\pos,[1,1,1,1]);
 Synth.new(\playmonobuf).set(\bufnum,19).set(\fadeTime,0.5).set(\amp,0.05).set(\rate,20.0).set(\dur,2.0).set(\pos,[1,1,1,1]);
			}, {
		});
	});
	// 3
	if(index == ~midiPads[2][2], {
		if(type == 1, {
			~buf2_in = Synth.new(\AudioIn).set(\fadeTime,5).set(\amp,0.0).set(\pos,[1,1,1,1]).set(\in,6);
			~synths = ~synths.add([2,index,`~buf2_in]);
			}, {
				"toast".postln;
			~buf2_in.release();
		});
	});

	// 4
	if(index == ~midiPads[2][3], {
		if(type == 1, {
			~stopAllSynths.value();
			Synth(\SOSkick, [\sustain, 1.0, \beater_noise_level, 0.01, \mod_freq, 15, \amp, 0.225]);
			}, {
		});
	});

	// 5
	if(index == ~midiPads[2][4], {
		if(type == 1, {
			~buf2_5 = Synth.new(\playmonobufloop).set(\bufnum,9).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([2,index,`~buf2_5]);
			}, {
				~buf2_5.release();
		});
	});
	// 6
	if(index == ~midiPads[2][5], {
		if(type == 1, {
			~buf2_6 = Synth.new(\playmonobufloop).set(\bufnum,10).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([2,index,`~buf2_6]);
			}, {
				~buf2_6.release();
		});
	});
	// 7
	if(index == ~midiPads[2][6], {
		if(type == 1, {
			~buf2_7 = Synth.new(\playmonobufloop).set(\bufnum,11).set(\fadeTime,5).set(\amp,0.0).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([2,index,`~buf2_7]);
			}, {
				~buf2_7.release();
		});
	});
	// 8
	if(index == ~midiPads[2][7], {
		if(type == 1, {
			~lowBeat = Synth.new(\lowBeat).set(\fadeTime, 2.0).set(\amp,0.0);
			~synths = ~synths.add([2,index,`~lowBeat]);
			}, {
			~lowBeat.release;
		});
	});

	// knobs
	// K1
	if(index == ~midiKnobs[2][0], {
		~buf2_5.set(\rate, LinLin.kr(value.asFloat, 0, 127, -1, 1));
	});
	// K2
	if(index == ~midiKnobs[2][1], {
		~buf2_6.set(\rate, LinLin.kr(value.asFloat, 0, 127, -1, 1));
	});
	// K3
	if(index == ~midiKnobs[2][2], {
		~buf2_7.set(\rate, LinLin.kr(value.asFloat, 0, 127, -1, 1));
	});
	// K4
	if(index == ~midiKnobs[2][3], {
		~lowBeat.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 4));
	});
	// K5
	if(index == ~midiKnobs[2][4], {
		~buf2_5.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});
	// K6
	if(index == ~midiKnobs[2][5], {
		~buf2_6.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});
	// K7
	if(index == ~midiKnobs[2][6], {
		~buf2_7.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});
	// K8
	if(index == ~midiKnobs[2][7], {
		~buf2_in.set(\amp, LinLin.kr(value.asFloat, 0, 127, 0, 1));
	});
};


// CHANNEL 3
~midiManager3 = { arg type, index, value;
	// pads
	// 1
	~midiPads[3][0].post; " is ".post; index.postln;
	if(index == ~midiPads[3][0], {
		if(type == 1, {
			~buf3_1 = Synth.new(\playmonobufloop).set(\bufnum,12).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_1]);
			}, {
				~buf3_1.release();
		});
	});
	// 2
	if(index == ~midiPads[3][1], {
		if(type == 1, {
			~buf3_2 = Synth.new(\playmonobufloop).set(\bufnum,13).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_2]);
			}, {
				~buf3_2.release();
		});
	});
	// 3
	if(index == ~midiPads[3][2], {
		if(type == 1, {
			~buf3_3 = Synth.new(\playmonobufloop).set(\bufnum,14).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.25).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_3]);
			}, {
				~buf3_3.release();
		});
	});

	// 4
	if(index == ~midiPads[3][3], {
		if(type == 1, {
			~buf3_4 = Synth.new(\playmonobufloop).set(\bufnum,17).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_4]);
			}, {
				~buf3_4.release();
		});
	});

	// 5
	if(index == ~midiPads[3][4], {
		if(type == 1, {
			~buf3_5 = Synth.new(\playmonobufloop).set(\bufnum,18).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_5]);
			}, {
				~buf3_5.release();
		});
	});
	// 6
	if(index == ~midiPads[3][5], {
		if(type == 1, {
			~buf3_6 = Synth.new(\playmonobufloop).set(\bufnum,19).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\rate,1.0).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_6]);
			}, {
				~buf3_6.release();
		});
	});
	// 7
	if(index == ~midiPads[3][6], {
		if(type == 1, {
			~buf3_7 = Synth.new(\nice).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_7]);
			}, {
				~buf3_7.release();
		});
	});
	// 8
	if(index == ~midiPads[3][7], {
		if(type == 1, {
			~buf3_8 = Synth.new(\walker).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_8]);
			}, {
				~buf3_8.release();
		});
	});
	if(index == ~midiPads[3][8], {
		if(type == 1, {
			~buf3_9 = Synth.new(\walker2).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_9]);
			}, {
			~buf3_9.release();
		});
	});
	if(index == ~midiPads[3][9], {
		if(type == 1, {
			~buf3_10 = Synth.new(\walker3).set(\fadeTime,LinLin.kr(value.asFloat, 0, 127, 10, 1)).set(\amp,0.125).set(\pos,[1,1,1,1]);
			~synths = ~synths.add([1,index,`~buf3_10]);
			}, {
			~buf3_10.release();
		});
	});
	if(index == ~midiPads[3][14], {
		if(type == 1, {
			~pad2.do({ arg item, i;
				~synths = ~synths.add([1,~midiPads[1][5],`(~pad2[i])]);
			});
			~synths.do({ arg item, i;
				var chan = item[0];
				var num = item[1];
				var veloc = 0;
				item[2].value.set(\fadeTime, 2.5);
				~noteOff.value(chan,num,veloc);
				~launchControl.noteOn(chan,num,'29'); // red l red'13' amber'29' green'28'
			});
			~synths = [];
			}, {
		});
	});
};


// getting midi
MIDIClient.init;
MIDIIn.connectAll;
// MIDIClient.init;
MIDIClient.destinations;
// ~launchControl.free;
// ~launchControl = MIDIOut.newByName("Launch Control", "Launch Control").latency_(Server.default.latency);

// ~lpd8.free;
// ~lpd8 = MIDIOut.newByName("LPD8", "LPD8").latency_(Server.default.latency);

~midiInOn = MIDIFunc.noteOn({ |veloc, num, chan, src|
	if( ~midiVerbose == True, {
		"ON ".post;
		chan.post;
		" : ".post;
		num.post;
		" : ".post;
		veloc.postln;
	});
	if(chan == 1, {
		~midiManager1.value(1,num,veloc);
	});
	if(chan == 2, {
		~midiManager2.value(1,num,veloc);
	});
	if(chan == 3, {
		~midiManager3.value(1,num,veloc);
	});

	if(chan == 0, {
		~midiManager0.value(1,num,veloc);
		if( num%3==0, {
			~launchControl.noteOn(chan,num,'15'); // red f red'15' amber'63' green'60'
		});
		if( num%3==1, {
			~launchControl.noteOn(chan,num,'15'); // amber f
		});
		if( num%3==2, {
			~launchControl.noteOn(chan,num,'15'); // green f
		});
	});
});

~noteOff = { arg chan, num, veloc;
	if( ~midiVerbose == True, {
		"OFF ".post;
		chan.post;
		" : ".post;
		num.post;
		" : ".post;
		veloc.postln;
	});
	if(chan == 0, {
		~midiManager0.value(0,num,veloc);
		if( num%3==0, {
			~launchControl.noteOn(chan,num,'28'); // red l red'13' amber'29' green'28'
			// ~launchControl.sysex(Int8Array[0xf0, 0x00, 0x20, 0x29, 0x02, 0x11, 0x7B, 0x01, 0x09, 0x00, 0xf7]);
		});
		if( num%3==1, {
			~launchControl.noteOn(chan,num,'28'); // amber l
		});
		if( num%3==2, {
			~launchControl.noteOn(chan,num,'28'); // green l
		});
	});
	if(chan == 1, {
		~midiManager1.value(0,num,veloc);
	});
	if(chan == 2, {
		~midiManager2.value(0,num,veloc);
	});
	if(chan == 3, {
		~midiManager3.value(0,num,veloc);
	});
};

~midiInOff = MIDIFunc.noteOff({ |veloc, num, chan, src|
	~noteOff.value(chan, num, veloc);
});

~midiInCC = MIDIFunc.cc({ | func, ccNum, chan, srcID, argTemplate, dispatcher|
	if( ~midiVerbose == True, {
		"CC ".post;
		chan.post;
		" : ".post;
		ccNum.post;
		" : ".post;
		func.postln;
	});
	if(chan == 0, {
		~midiManager0.value(2,ccNum,func);
	});
	if(chan == 1, {
		~midiManager1.value(2,ccNum,func);
	});
	if(chan == 2, {
		~midiManager2.value(2,ccNum,func);
	});
	if(chan == 3, {
		~midiManager3.value(2,ccNum,func);
	});
});

)
// when done:
q.value;