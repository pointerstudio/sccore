// Rumush
// https://mycelialcordsblog.wordpress.com/
// https://fungorumush.bandcamp.com/releases
// https://soundcloud.com/fungorum
(
f = {
	var rep = [4, 8, 16, 32];
	var n = rep.choose;
	var x = [

	      54, 60, 66, 72, 81, 90, 96, 102,
	      108, 128, 132, 144, 162, 180, 192, 204,
	      216, 240, 264, 288, 324, 360, 384, 408,
	      432, 480, 528, 576, 648, 720, 768, 816,
	      864, 960, 1056, 1152, 1296, 1440, 1536, 1632,
	      1728, 1920, 2112, 2304, 2592, 2880, 3072, 3264,
	      3456, 3840, 4224, 4608, 5184, 5760, 6144, 6528,
	      6912, 7680, 8448, 9216, 10368, 11520, 12288, 528

       ];
	Mix.fill(n,
		{
			var detune = 5.rand;
			var sin = SinOsc.ar(x.choose, 4.rand, 0.25);
			var saw = RLPF.ar(Saw.ar(x.choose*0.01+detune, 0.75), x.choose, 2.rand).tanh;
			Pan2.ar((sin+saw) * EnvGen.kr(Env.sine(12, 1/n), 1, doneAction:2),
			1.rand2) }
	);
};
)

(
// DEFINE A ROUTINE
r = Routine({

    inf.do({
		f.play;
        10.wait;
    })
});
)

r.reset.play;

r.stop;

// Rumush
// Facebook: https://www.facebook.com/rumushproduction
// SoundCloud: https://soundcloud.com/rumushproduction
// YouTube: https://www.youtube.com/channel/UCs_Cn1R4iFrYOyc8liFucSQ
// Blog: https://mycelialcordsblog.wordpress.com/
// GitHub: https://github.com/RumushMycelialCords
/*
BPF Experiment
*/
(
{// Noise Filter Experiment - Random
	var src=PinkNoise.ar();
	16.do{src=BRF.ar(src,rrand(100,15000)!2)};
	src
}.play
)
(
{// Noise Filter Experiment - Random with LFNoise1
 	var src=PinkNoise.ar();
	var lfo = {arg rt; LFNoise1.ar(rt!2)};
	16.do{src=BRF.ar(src,rrand(100,15000)*(0.75+(lfo.(8.0.rand)*0.25)),lfo.(8.0.rand).range(0.05,1))};
	src
}.play
)
(
{// Noise Filter Experiment - Random with LFPulse
 	var src=PinkNoise.ar();
	var lfo = {arg rt; LFPulse.ar(rt!2)};
	16.do{src=BRF.ar(src,rrand(100,15000)*(0.75+(lfo.(0.5.rand)*0.25)),lfo.(0.5.rand).range(0.05,1))};
	src
}.play
)
(
{// Noise BPF - Creative Use
	var src=PinkNoise.ar();
	var sawRt = 0.5;
	var lfo = {arg rt, rt2; LFPulse.ar(
		[rt+SinOsc.ar(rt2.rand,0,rrand(rt/2,rt*2)),rt+SinOsc.ar(rt2.rand,0,rrand(rt/2,rt*2))
	])};
	8.do{src=BRF.ar(
		src,rrand(250,15000)*(0.5+(lfo.(lfo.(1,2)*LFSaw.ar(sawRt).abs*5000,0.25)*0.5)),
		LFSaw.ar(sawRt*rrand(0.5,0.75)).abs*0.5+0.25)
	};
	CombC.ar(
		(src*LFSaw.ar(sawRt*rrand(0.25,0.5),0,8).abs).tanh,
		0.3,0.25+LFSaw.ar([sawRt*0.95,sawRt*0.93],0,0.05).abs,
		4,0.5
	).tanh
}.play
)

(
{// Noise BPF - Creative Use
	var freq = 50;
	var rt = 0.5;
	var src = PinkNoise.ar(1!2);
	4.do{src=BRF.ar(src,rrand(100,500).round(10)+(freq*LFNoise1.ar(rt.rand!2)),0.1)};
	4.do{src=BRF.ar(src,rrand(1000,2500).round(10)+(freq*LFNoise1.ar(rt.rand!2)),0.2)};
	4.do{src=BRF.ar(src,rrand(250,750).round(10)+(freq*LFNoise1.ar(rt.rand!2)),0.1)};
	4.do{src=BRF.ar(src,rrand(2500,5000).round(10)+(freq*LFNoise1.ar(rt.rand!2)),0.2)};
	src=FreqShift.ar(src,Duty.ar(8,0,Dwhite(-1*freq*10,freq*10).round(freq/2)),0,0.25)+src;
	src=CombC.ar(src,1,1,8);
	Limiter.ar(LPF.ar(src,2500+LFNoise1.ar(rt.rand!2,1250)))
}.play
)

// Rumush
// Facebook: https://www.facebook.com/rumushproduction
// SoundCloud: https://soundcloud.com/rumushproduction
// YouTube: https://www.youtube.com/channel/UCs_Cn1R4iFrYOyc8liFucSQ
// Blog: https://mycelialcordsblog.wordpress.com/
// GitHub: https://github.com/RumushMycelialCords

(
SynthDef(\fdb1, {
	var src, loc, freq, rt;
	rt = 0.25;
	freq = Duty.ar(16/rt,0,Dseq([60,68,67,59,61,64,65].midicps,inf));

	src = Saw.ar(freq)*Decay.ar(Impulse.ar(rt),1/rt,0.25);
	//src = SoundIn.ar(0);
	loc = LocalIn.ar(2)+src;
	loc = FreqShift.ar(loc,-1);
	loc = loc+DelayC.ar(loc,0.2,freq.reciprocal);
	loc = DelayC.ar(loc,4,LFNoise1.ar(rt!2).range(0.25,2));
	loc = DelayC.ar(loc,4,2);
	//loc = (loc*250).tanh;
	//loc = LPF.ar(loc,2500);
	loc = loc+AllpassC.ar(loc,0.1,LFNoise0.ar(rt!2).range(0.05,0.1),4);
	loc = HPF.ar(loc,100);

	LocalOut.ar(loc*1.75);

	Out.ar(0,Limiter.ar(loc)*0.5*EnvGate.new);
}).add;

Server.killAll

~ndef = Synth.new(\fdb1).set(\fadeTime,5.0);
~ndef.release();

)


// Rumush
// Facebook: https://www.facebook.com/rumushproduction
// SoundCloud: https://soundcloud.com/rumushproduction
// YouTube: https://www.youtube.com/channel/UCs_Cn1R4iFrYOyc8liFucSQ
// Blog: https://mycelialcordsblog.wordpress.com/
// GitHub: https://github.com/RumushMycelialCords

(// SynthDef and Ndef
~tp = 145/60; // 145 BPM
SynthDef(\sin, {arg freq=50, lw=0, index=1000, lpF=20000, hpF=40, mix=0.75, dur, amp=0.125, pan=0, out=0;
	var env = {arg dr, ml; Decay.ar(Impulse.ar(0),dr,ml)};
	var src1 = SinOsc.ar(
		freq+env.(dur*Rand(0.05,0.125),freq*(IRand(4,16))),2pi,
		env.(dur,amp*mix)
	);
	var src2 = SinOscFB.ar(
		freq+(src1*index),Rand(lw,2),env.(dur*Rand(0.05,1),amp*(1-mix))
	);
	var src = Pan2.ar(src1+src2,pan);
	var free = DetectSilence.ar(src,doneAction:2);
	src = LPF.ar(src,lpF);
	src = HPF.ar(src,hpF);

	Out.ar(out,Compander.ar(src,src,0.25,1,1/4,0.01,0.1))
}).store;
Ndef(\delay, {
	var src = In.ar(2,2);
	var loc = LocalIn.ar(2)+src;
	loc = DelayC.ar(loc,1/~tp*2,LFNoise0.ar(~tp).range(0.125,1).round(0.25)/~tp);
	loc = FreqShift.ar(loc,LFNoise1.ar(3/~tp).range(-15,15));
	loc = loc+(loc ring4: WhiteNoise.ar(1*LFNoise1.ar(0.25)));

	LocalOut.ar(loc*0.5);
	Pan2.ar(loc,SinOsc.ar(2))
}).play
)
(
{
	var kick, snare, hihat, noise, fill1, rh1;
	fill1 = Pseq([Pseq([1],4),Pseq([0.125],8)],inf); // Fill used with a snare
	rh1 = Pwrand([0.5,1,0.25,0.125],[0.75,0.15,0.05,0.05],inf); // Different hihat rhythm
	~freq = 60; // Fundamental Frequency
	~bar = 1.0/~tp*16; //
	kick = Pbind(*[
		\instrument, \sin,
		freq: ~freq,
		lw: 0,
		index: 10,
		lpF: 20000,
		hpF: 40,
		mix: Pwhite(0.75,1),
		dur: 1/~tp,
		amp: 0.75,
		pan: 0,
		out: 0
	]);
	snare = Pbind(*[
		\instrument, \sin,
		freq: Pseq([\rest,~freq],inf),
		lw: 1.5,
		index: Pwhite(5000,7500),
		lpF: 15000,
		hpF: 100,
		mix: Pwhite(0.25,0.5),
		dur: 1/~tp,
		amp: 0.3,
		pan: 0,
		out: 0
	]);
	hihat = Pbind(*[
		\instrument, \sin,
		freq: ~freq,
		lw: 1.5,
		index: Pwhite(5000,15000),
		lpF: 17500,
		hpF: 500,
		mix: Pwhite(0,0.5),
		dur: 0.5/~tp,
		amp: 0.125,
		pan: Pwhite(-1.0,1.0),
		out: Pwrand([0,2],[0.95,0.05],inf)
	]);
	noise = Pbind(*[
		\instrument, \sin,
		freq: ~freq*4,
		lw: 1,
		index: Pwhite(150,300),
		lpF: 15000,
		hpF: 2500,
		mix: Pwhite(0,1),
		dur: 4/~tp,
		amp: 0.1,
		pan: 0,
		out: 2
	]);
	Pdef(\kick, kick).play; ~bar.wait;
	Pdef(\snare, snare).play; ~bar.wait;
	Pdef(\hihat, hihat).play; ~bar.wait;
	Pdef(\hihat2, hihat).play;
	Pbindef(\hihat2, \dur, 0.75/~tp); ~bar.wait;
	Pdef(\hihat3, hihat).play;
	Pbindef(\hihat3, \dur, 0.25/~tp, \amp, 0.125*Pseq([1,0.5],inf)); ~bar.wait;
	Pbindef(\snare, \dur, fill1/~tp, \freq, ~freq); ~bar.wait;
	Pbindef(\snare, \dur, 1/~tp);
	Pdef(\kick).pause; ~bar.wait;
	Pdef(\kick, kick).play;
	Pdef(\noise, noise).play;
	Pbindef(\hihat2, \dur, rh1/~tp, \freq, ~freq*4);

}.fork
)